#!/bin/sh
if [ $# != 1 ]
then
    echo $0: usage: $0 resultdir >&2
    exit 1
fi
    
res=$1
ctr=0
find ~/saved-images -type f\
|sort\
|while read fn
 do
  n=$res/`printf %03d $ctr`.`basename $fn`.pnm;djpeg $fn \
     | pamscale -xysize 1024 768  >"$n" 
  ctr=`expr $ctr + 1`
done
