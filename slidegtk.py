#!/usr/bin/env python3

import re
import os
import sys
import time
import getopt
import subprocess

from gi import pygtkcompat
pygtkcompat.enable()
pygtkcompat.enable_gtk(version='3.0')
import gtk
import gobject
import glib

class ImageCache(object):
    def __init__(self, base, width, height):
        if not os.path.isdir(base):
            os.makedirs(base)
        self.__base = base
        self.width, self.height = width, height
        self.__lru = []

    def same_size(self, size):
        return self.width == size.width and self.height == size.height

    def compute_filename(self, infile, rotation):
        return os.path.join(self.__base,
                            "%d-%d-%d-%s" % (
                self.width, self.height,
                rotation, infile.replace('/', '-')))

    def processed_file(self, infile, rotation = 0):
        if rotation == 0:
            fp = os.popen("identify -format \"%w %h\" '" + infile + "'")
            words = fp.read().split()
            fp.close()
            w = int(words[0])
            h = int(words[1])
            if w <= self.width and h <= self.height:
                return infile

        fn = self.compute_filename(infile, rotation)
        try:
            self.__lru.remove(fn)
        except ValueError:
            pass
        self.__lru.append(fn)
        while len(self.__lru) > 30:
            os.remove(self.__lru[0])
            del self.__lru[0]
        if not os.path.exists(fn):
            if rotation == 0:
                os.system("djpeg '%s'"
                          "|pnmscale -xysize %s %s|cjpeg -optimize > '%s'" % (
                              infile, self.width, self.height,
                              fn))
            else:
                os.system("jpegtran -rotate %s %s"
                          "|djpeg"
                          "|pnmscale -xysize %s %s|cjpeg -optimize > %s" % (
                              rotation, subprocess.mkarg(infile),
                              self.width, self.height,
                              subprocess.mkarg(fn)))
        return fn

    def hint(self, infile, rotation = 0):
        self.processed_file(infile, rotation)

    def hint_images(self, images):
        self.all_wanted_images = set()
        for img in images:
            self.all_wanted_images.add(self.compute_filename(img, 0))

class ImageViewer(object):
    def loadimage(self):
        timer = self.timer
        if timer is not None:
            self.cancel_timer()
        fn = self.images[self.index]
        self.image.set_from_file(self.cache.processed_file(fn))
        if fn in self.stopimages:
            self.stopimages.discard(fn)
        elif timer is not None:
            self.start_timer()
        self.cancel_hint_timer()
        self.hint_timer = glib.timeout_add(10, self.hint_cache)

    def hint_cache(self):
        self.cancel_hint_timer()
        if self.index+1 < len(self.images):
            self.cache.hint(self.images[self.index+1])

    def cancel_hint_timer(self):
        if self.hint_timer is not None:
            glib.source_remove(self.hint_timer)
            self.hint_timer = None

    def nextimage(self):
        self.index += 1
        if self.index >= len(self.images):
            if self.autorepeat:
                self.index = 0
            else:
                self.index = len(self.images) - 1
        self.loadimage()
        return True

    def previmage(self):
        self.index -= 1
        if self.index < 0:
            if self.autorepeat:
                self.index = len(self.images) - 1
            else:
                self.index = 0
        self.loadimage()
        return True

    def button_click(self, widget, event):
        if event.button == 1:
            self.nextimage()
            return True
        if event.button in [2, 3]:
            self.previmage()
            return True
        return False

    def dwim(self):
        if self.speed is not None or self.endtime is not None:
            if self.timer is None:
                self.start_timer()
            else:
                self.cancel_timer()
        else:
            self.nextimage()

    def key_press(self, widget, event):
        if event.string == " ":
            self.dwim()
            return True
        if event.string == "<":
            self.cancel_timer()
            self.index = 0
            self.loadimage()
            return True
        if event.string == ">":
            self.index = len(self.images) - 1
            self.loadimage()
            return True
        if event.keyval in [gtk.keysyms.BackSpace, gtk.keysyms.Delete]:
            self.previmage()
            self.cancel_timer()
            return True
        if event.string == "p":
            self.cancel_timer()
            self.previmage()
            return True
        if event.string == "n":
            self.cancel_timer()
            self.nextimage()
            return True
        if event.string == "q":
            gtk.main_quit()
            return True
        if event.string == "i":
            print(self.images[self.index])
        if event.keyval == gtk.keysyms.Left:
            self.previmage()
            self.cancel_timer()
            return True
        if event.keyval == gtk.keysyms.Right:
            self.nextimage()
            self.cancel_timer()
            return True
        if event.keyval == gtk.keysyms.Up:
            if self.speed is None:
                self.speed = 9
            else:
                self.speed -= 1
                if self.speed == 0:
                    self.speed = 1
            self.start_timer()
            return True
        if event.keyval == gtk.keysyms.Down:
            if self.speed is None:
                self.speed = 1
            else:
                self.speed += 1
            self.start_timer()
            return True
        if event.string == "0":
            self.speed = None
            self.cancel_timer()
            return True
        if event.string in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            self.speed = int(event.string)
            self.start_timer()
            return True
        if event.string == "a":
            self.autorepeat = True
            return True
        if event.string == "A":
            self.autorepeat = False
            return True

        return False

    def start_timer(self):
        self.cancel_timer()
        if self.speed is not None:
            period = 0.5 * self.speed
        else:
            timeleft = self.endtime - time.time()
            imgleft = len(self.images) - self.index
            if imgleft == 0:
                period = 1
            else:
                period = timeleft / imgleft
                if period < 0.2:
                    period = 0.2
        # gtk counts in milliseconds, not seconds.
        period *= 1000
        self.timer = glib.timeout_add(int(period), self.nextimage)

    def cancel_timer(self):
        if self.timer is not None:
            glib.source_remove(self.timer)
            self.timer = None

    def close_application(self, widget, event, data=None):
        gtk.main_quit()
        return gtk.FALSE

    def size_allocate(self, widget, size):

        if self.cache is not None and self.cache.same_size(size):
            return

        self.cache = ImageCache("/tmp/slidegtkcache", 
                                size.width, 
                                size.height)
        self.cache.hint_images(self.images)
        self.loadimage()
        if self.__fullscreen:
            fp = open("/tmp/natural-fullsize.txt", "w")
            fp.write("%d %d\n" % (size.width, size.height))
            fp.close()

    def __init__(self, images, stopimages, fullscreen, width, height):
        self.cache = None
        self.hint_timer = None
        self.speed = None
        self.timer = None
        self.endtime = None
        self.stopimages = stopimages
        self.autorepeat = False
        w = gtk.Window(type=gtk.WINDOW_TOPLEVEL)
        w.set_title("slidegtk")
        self.__fullscreen = fullscreen
        if fullscreen == 1:
            w.set_size_request(1920, 1200)
        elif fullscreen:
            w.fullscreen()
        else:
            w.set_size_request(width, height)
            w.move(0, 0)
        w.connect("size-allocate", self.size_allocate)

        w.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(red=255, green=0, blue=0))
        w.connect("delete_event", self.close_application)
        w.set_events(w.get_events()
                     | gtk.gdk.BUTTON_PRESS_MASK
                     | gtk.gdk.KEY_PRESS_MASK)
        w.connect("button_press_event", self.button_click)
        w.connect("key_press_event", self.key_press)
        self.window = w

        img = gtk.Image()
        w.add(img)
        self.image = img
        self.images = images
        self.index = 0
        img.show()
        w.show()
        if not fullscreen or fullscreen == 1:
            w.move(0, -21)
        if fullscreen:
            self.disable_screensaver()

    def disable_screensaver(self):
        try:
            import dbus
        except ImportError:
            return

        DBUS_BUS_NAME = "org.gnome.ScreenSaver"
        DBUS_OBJECT_PATH = "/org/gnome/ScreenSaver"
        DBUS_INTERFACE = "org.gnome.ScreenSaver"

        bus = dbus.SessionBus()
        try:
            self.dbus_screensaver_iface = dbus.Interface(
                bus.get_object( DBUS_BUS_NAME, DBUS_OBJECT_PATH ),
                DBUS_INTERFACE )
            self.dbus_screensaver_iface.Inhibit("slidegtk", "showing slideshow")
        except dbus.exceptions.DBusException:
            pass

    def set_endtime(self, endtime):
        self.endtime = endtime
        self.start_timer()

def main(argv):
    endtime = None
    stopimages = set()
    width, height = 640, 480
    use_stdin = False
    fullscreen = False

    opts, args = getopt.getopt(argv[1:], 'fg:', [
            "minutes=", "seconds=", "stop=", "crt", "fullscreen", "stdin",
            "geometry=", "compat"])


    for opt, arg in opts:
        if opt == "--minutes":
            endtime = time.time() + 60 * int(arg)
        elif opt == "--seconds":
            endtime = time.time() + int(arg)
        elif opt == "--stop":
            stopimages.add(arg)
        elif opt == "--crt":
            width, height = 1024, 768
        elif opt == "--stdin":
            use_stdin = True
        elif opt == '-g' or opt == '--geometry':
            m = re.match("^(?P<width>[1-9][0-9]*)"
                         "x(?P<height>[1-9][0-9]*)$", arg)
            if m is None:
                usage()
            width = int(m.group("width"))
            height = int(m.group("height"))
        elif opt == '--fullscreen' or opt == '-f':
            fullscreen = True
        elif opt == "--compat":
            fullscreen = 1
        else:
            usage()

    if use_stdin:
        images = sys.stdin.read().strip().split("\n")
    else:
        images = args

    v = ImageViewer(images, stopimages, fullscreen, width, height)
    if endtime is not None:
        v.set_endtime(endtime)
    gtk.main()

if __name__ == "__main__":
    main(sys.argv)
